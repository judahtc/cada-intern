from django.shortcuts import render
from store.models import users
from store.models import cart_items
from store.models import cart

# Create your views here.

def home(request):
    return render(request,"home.html")


def login(request):
    return render(request,"login.html")   

def portal(request):
    if request.method=='POST':
        if users.objects.filter(username=request.POST['username'],password=request.POST['password']).exists():    

            request.session['username']= request.POST['username']
            username=request.session['username']
            
            all=users.objects.get(username=request.POST['username'])
            all1=cart.objects.filter(clientname=username).count()
            
            context={'data':all,'data2':all1}
            return render(request,"portal.html",context)
        else:
            context={'data':"invalid username or password"}  
            return render(request,"login.html",context)  

def mainPortal(request):
    username=request.session['username']
    all1=users.objects.get(username=username)
    conte={'data':all1}
    return render(request,"mainPortal.html",conte)   

def router(request):
    username=request.session['username']
    all2=users.objects.get(username=username)
    all1=cart_items.objects.get(id=1)
    conte={'data':all2,'data1':all1 }
    return render(request,"router.html",conte) 

def mifi(request):
    username=request.session['username']
    all2=users.objects.get(username=username)
    all1=cart_items.objects.get(id=6)
    conte={'data':all2,'data1':all1 }
    return render(request,"mifi.html",conte) 




def regUser(request):
    if request.method=='POST':
        if request.POST.get('username') and request.POST.get('password') and request.POST.get('email') and request.POST.get('contact') and request.POST.get('dob'):
            user=users()
            user.username=request.POST.get('username')
            user.password=request.POST.get('password')
            user.email=request.POST.get('email')
            user.contact=request.POST.get('contact')
            user.dob=request.POST.get('username')
            user.save()
            
            return render(request,"userReg.html") 

            
    else:        
        return render(request,"userReg.html")
        

def cartItems(request):
    all=cart_items.objects.all()
    contex={'data':all}
    return render(request,"items.html",contex)

def deleteItems(request,id):
    all1=cart_items.objects.get(id=id)
    all1.delete()

    all=cart_items.objects.all()
    contex={'data':all}
    return render(request,"items.html",contex)

def addRouter(request):
    username=request.session['username']
    all=cart_items.objects.get(id=1)
    all1=cart_items.objects.get(id=1)
    all2=users.objects.get(username=username)
    
    Cart=cart()
    Cart.clientname=username
    Cart.item_name=all.item_name  
    Cart.item_id=all.item_id
    

    Cart.save()

    all2=users.objects.get(username=username)
    all1=cart_items.objects.get(id=1)
    conte={'data':all2,'data1':all1,'data2':"item successfully added to cart" }
    return render(request,"router.html",conte) 


def addMifi(request):
    username=request.session['username']
    all=cart_items.objects.get(id=6)
    all1=cart_items.objects.get(id=6)
    all2=users.objects.get(username=username)
    
    Cart=cart()
    Cart.clientname=username
    Cart.item_name=all.item_name  
    Cart.item_id=all.item_id
    

    Cart.save()

    all2=users.objects.get(username=username)
    all1=cart_items.objects.get(id=6)
    conte={'data':all2,'data1':all1,'data2':"item successfully added to cart" }
    return render(request,"mifi.html",conte) 



def myCart(request):
    username=request.session['username']
    print(type(username))
    
    all=cart.objects.raw("SELECT * FROM cart WHERE clientname=%s ",[username])
    all1=cart_items.objects.raw("SELECT * FROM cart_items WHERE id=1 ")  
    alls=cart.objects.raw("SELECT t.id,t.item_id,t.item_name,t.clientname,ti.item_price from cart AS t join cart_items AS ti on t.item_id=ti.item_id where t.clientname=%s ",[username])
    all2=cart.objects.filter(clientname=username).count()
    conte={'data':alls,'data1':all1,'data2':all2,'data3':alls}
    return render(request,"myCart.html",conte) 
    
def delete(request,id):
    data=cart.objects.get(id=id)
    data.delete()

    username=request.session['username']
    all=cart.objects.filter(clientname=username)
    all1=cart_items.objects.get(id=1)
    alls=cart.objects.raw("SELECT t.id,t.item_id,t.item_name,t.clientname,ti.item_price from cart AS t join cart_items AS ti on t.item_id=ti.item_id where t.clientname=%s ",[username])
    all2=cart.objects.filter(clientname=username).count()
    conte={'data3':alls,'data1':all1,'data2':all2}
    return render(request,"myCart.html",conte) 
    

